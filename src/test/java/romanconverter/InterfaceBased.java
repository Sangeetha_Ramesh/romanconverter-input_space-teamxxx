package romanconverter;

import static org.junit.Assert.*;

import org.junit.Test;

import roman.RomanConverter;

public class InterfaceBased {

	@Test
	public void test() {
		//fail("Not yet implemented");
		RomanConverter roman = new RomanConverter();
		
		String r = " ";
		
		// Empty String
			
			try{
				roman.fromRoman(r);
			} catch( final IllegalArgumentException e ){
			    final String msg = "Invalid Roman numeral: "+r;
			    assertEquals(msg, e.getMessage());
			 }
			//inputs - relation with 0	
			
			try{
				roman.toRoman(123456);
				roman.toRoman(0);
				roman.toRoman(-3);
			} catch( final IllegalArgumentException e ){
			    final String msg = "number out of range (must be 1..3999)";
			    assertEquals(msg, e.getMessage());
			 }
	}

}
