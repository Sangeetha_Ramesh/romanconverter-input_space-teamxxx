package romanconverter;

import static org.junit.Assert.*;

import org.junit.Test;

import roman.RomanConverter;

public class FunctionalityBased {

	@Test
	public void test() {
		//fail("Not yet implemented");
		RomanConverter roman = new RomanConverter();
		
		assertEquals(5, roman.fromRoman("V"));
		assertEquals("V", roman.toRoman(5));
		
		String s = "asdfgf";
		// Cannot be tested for any values except an integer: double a = 4.6;
		
		//invalid inputs out of range	
				try{
					roman.toRoman(123456);
					roman.toRoman(0);
					roman.toRoman(-3);
				} catch( final IllegalArgumentException e ){
				    final String msg = "number out of range (must be 1..3999)";
				    assertEquals(msg, e.getMessage());
				 }
				
				//Invalid String	
				try{
					roman.fromRoman(s);
				} catch( final IllegalArgumentException e ){
				    final String msg = "Invalid Roman numeral: "+s;
				    assertEquals(msg, e.getMessage());
				 }
				
			//take a number, convert it to Roman numerals, 
			//then convert that back to a number, 
			//we should end up with the number we started with	
				for (int i = 1; i < 4000; i++)
				{
					String str = roman.toRoman(i);
					int rom = roman.fromRoman(str);
					assertEquals(i,rom);	
				}
	}

}
